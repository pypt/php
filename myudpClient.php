<?php  
/** 
 udp client 
**/  
class client{
	public $serve;
	function __construct(){
		$serve="udp://127.0.0.1:2001";
		echo "start a udp client\n";
	
		/* Open a socket to port 1234 on localhost */
		$socket = stream_socket_client($serve,$errno,$errstr);
		$pkt = stream_socket_recvfrom($socket, 1024, 0, $peer);
		echo $pkt;
		/* Send ordinary data via ordinary channels. */
		//fwrite($socket, "Normal data transmit.");
		//echo fread($socket, 26);
		/* Send more data out of band. */
		echo "send the message";
		echo stream_socket_sendto($socket, "Out of Band data.", STREAM_OOB);
		
		/* Close it up */
		echo stream_socket_shutdown($socket, STREAM_SHUT_WR);
	}
}
new client();
?>