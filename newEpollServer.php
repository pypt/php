<?php  
/** 
 newEpollServer
**/  
define('CONNECT_IN_MSG', chr(1));  
define('CONNECT_OUT_MSG', chr(2));  
  
  
class epoll{  
    private static $socket;  
    public static $connections;  
    private static $buffers;  
    //private static $msgs;  
    public static $fd2pid = array();  
    public static $blocked_ips = array();  
    //private static $link_msg;  
    function epoll($port, &$_msgs){//, &$_link_msg  
        //self::$msgs = $_msgs;  
        //self::$link_msg = $_link_msg;  
        if($port<1024) die("Port must be a number which bigger than 1024\n");  
        self::$socket = stream_socket_server ('tcp://0.0.0.0:'.$port, $errno, $errstr);  
        stream_set_blocking(self::$socket, 0);  
        $base = event_base_new();  
        $event = event_new();  
        event_set($event, self::$socket , EV_READ | EV_PERSIST, 'epoll::ev_accept', $base);  
        event_base_set($event, $base);  
        event_add($event);  
        event_base_loop($base);  
  
        self::$connections = array();  
        self::$buffers = array();  
        //echo __FILE__.'-'.__LINE__.'<br/>'.chr(10);  
    }  
    public static function ev_accept($socket, $flag, $base) {  
        //echo __FILE__.'-'.__LINE__.'<br/>'.chr(10);  
        static $id = 0;  
  
        $connection = stream_socket_accept($socket);  
        stream_set_blocking($connection, 0);  
        list($remoteIP, $remotePort) = explode(':',stream_socket_get_name($connection,true));  
          
        $id ++;  
  
        $buffer = event_buffer_new($connection, 'epoll::ev_read', NULL, 'epoll::ev_error', $id);  
        event_buffer_base_set($buffer, $base);  
        event_buffer_timeout_set($buffer, 30, 30);  
        event_buffer_watermark_set($buffer, EV_READ, 0, 0xffffff);  
        event_buffer_priority_set($buffer, 60);//超时自动断开时间  
        event_buffer_enable($buffer, EV_READ | EV_PERSIST);  
  
        // we need to save both buffer and connection outside  
        self::$connections[$id] = $connection;  
        self::$buffers[$id] = $buffer;  
        //self::$link_msg->send(CONNECT_IN_MSG.$remoteIP.','.$remotePort, $id);  
          
        self::$fd2pid[$id] = 0;  
        //echo 'In-> $id='.$id.',$connection='.$connection."\n";  
    }  
    public static function ev_error($buffer, $error, $id) {  
        event_buffer_disable(self::$buffers[$id], EV_READ | EV_WRITE);  
        event_buffer_free(self::$buffers[$id]);  
        //echo 'Ot-> $id='.$id."\n";  
        //self::$link_msg->send(CONNECT_OUT_MSG, $id);  
        fclose(self::$connections[$id]);  
        unset(self::$fd2pid[$id]);  
        unset(self::$buffers[$id], self::$connections[$id]);  
    }  
    public static function ev_read($buffer, $id) {  
        //echo __FILE__.'-'.__LINE__.'<br/>'.chr(10);  
        static $ct=0;  
        while ($read = event_buffer_read($buffer, 256))  
        {  
            $ct+=strlen($read);  
            if(strpos($read,'ct')!==false) echo 'Ct=>'.count(self::$connections).'\n';  
            $mid = self::$fd2pid[$id];  
            //self::$msgs[$mid]->send($read , $id);  
        }  
    }  
  
}  
?>